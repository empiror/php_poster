# 使用方法
```php
use atlaxy\Poster;
#默认大小
$img = new Poster();
# 宽度500 高度1000 背景颜色 RGB：244,244,244
//$img = new Poster(500,1000,"244,244,244");
#设置字体
$img->setFont($_SERVER['DOCUMENT_ROOT']."/font/PF.TTF");
#绘制文字 超过海报宽度自动截取 文字内不要换行
$img->drawSingleLineText("我是chinese,你好啊，こんにちは、我是chinese,你好啊，こんにちは、",24,10,10);
#绘制图片
$img->drawImage("https://xxpt.vshop365.cn/storage/upload/6e/2ef680d1a60a95227eae226269d6dd.jpg",0,60,580,580);
$img->drawImage("https://xxpt.vshop365.cn/QrCode/user-coupon-16.png",300,650,230,230);
#绘制文字 设置最大右侧坐标 
$img->drawSingleLineText("我是chinese,你好啊，こんにちは、我是chinese,你好啊，こんにちは、",24,10,710,"0,0,0",300);
$img->savePng($_SERVER['DOCUMENT_ROOT']."/test.png");
```
- 多行文本的 后续加入

![avatar](http://xxpt.vshop365.cn/test.png)